################################################################################
#
# ember_vdr
#
################################################################################

EMBER_VDR_VERSION = $(VDR_VERSION)
EMBER_VDR_SITE = $(BR2_EXTERNAL)/package/ember_vdr/src
EMBER_VDR_SITE_METHOD = local
EMBER_VDR_INSTALL_STAGING = NO
EMBER_VDR_INSTALL_TARGET = YES

EMBER_VDR_DEPENDENCIES = vdr
EMBER_VDR_PK_NAME = "VDR"
EMBER_VDR_PK_VERSION = 0.0.1
EMBER_VDR_PK_DESCRIPTION = "A powerful DVB TV application for decoding DVB-T and DVB-S video transmissions. VDR is capible of transmitting these broadcasts over your connected local network."
EMBER_VDR_PK_DEPENDENCIES = ""
EMBER_VDR_PK_REQUIREDFW = "4.0.1"
EMBER_VDR_PK_DOWNLOAD = "package-VDR.tar.gz"
EMBER_VDR_PK_INITSCRIPT = "PVDR"



ifeq ($(BR2_ARCH),"arm")
PACKAGE_ARCH = armv7l
else
PACKAGE_ARCH = $(BR2_ARCH)
endif
OUTPUTDIR = $(BASE_DIR)/../../PACKAGES-$(PACKAGE_ARCH)

define EMBER_VDR_INSTALL_LIBS
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/$(call qstrip,$(EMBER_VDR_PK_NAME))/libs/vdr
	cp -rfv $(TARGET_DIR)/usr/lib/libcap* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/$(call qstrip,$(EMBER_VDR_PK_NAME))/libs
	cp -rfv $(TARGET_DIR)/usr/lib/libdvbcsa* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/$(call qstrip,$(EMBER_VDR_PK_NAME))/libs
	#cp -rfv $(TARGET_DIR)/usr/lib/libtntnet* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/$(call qstrip,$(EMBER_VDR_PK_NAME))/libs
	cp -rfv $(TARGET_DIR)/usr/lib/vdr/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/$(call qstrip,$(EMBER_VDR_PK_NAME))/libs/vdr
endef

define EMBER_VDR_INSTALL_BINARIES
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/bin
	cp -fv $(TARGET_DIR)/usr/bin/vdr $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/bin
	cp -fv $(TARGET_DIR)/usr/bin/svdrpsend $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/bin
	cp -fv $(TARGET_DIR)/usr/bin/ngettext $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/bin
	cp -fv $(TARGET_DIR)/usr/bin/gettext $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/bin
	cp -fv $(TARGET_DIR)/usr/bin/envsubst $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/bin
	$(INSTALL) -m 0755 -D $(BR2_EXTERNAL)/package/ember_vdr/runvdr $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/bin/runvdr
endef

define EMBER_VDR_INSTALL_TARGET_CMDS
	mkdir -p $(OUTPUTDIR)
	rm -rf $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/$(call qstrip,$(EMBER_VDR_PK_NAME))
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Run

	echo '[$(call qstrip,$(EMBER_VDR_PK_NAME))]' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/$(call qstrip,$(EMBER_VDR_PK_NAME))/package_info
	echo 'name = $(EMBER_VDR_PK_NAME)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/$(call qstrip,$(EMBER_VDR_PK_NAME))/package_info
	echo 'description = $(EMBER_VDR_PK_DESCRIPTION)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/$(call qstrip,$(EMBER_VDR_PK_NAME))/package_info
	echo 'version = "$(EMBER_VDR_PK_VERSION)"' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/$(call qstrip,$(EMBER_VDR_PK_NAME))/package_info
	echo 'dependencies = $(EMBER_VDR_PK_DEPENDENCIES)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/$(call qstrip,$(EMBER_VDR_PK_NAME))/package_info
	echo 'requiredFW = $(EMBER_VDR_PK_REQUIREDFW)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/$(call qstrip,$(EMBER_VDR_PK_NAME))/package_info
	echo 'download = $(EMBER_VDR_PK_DOWNLOAD)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/$(call qstrip,$(EMBER_VDR_PK_NAME))/package_info

	$(EMBER_VDR_INSTALL_BINARIES)
	$(EMBER_VDR_INSTALL_LIBS)
	cp -rfv $(BR2_EXTERNAL)/package/ember_vdr/src/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Programs/$(call qstrip,$(EMBER_VDR_PK_NAME))
	$(INSTALL) -m 0755 -D $(BR2_EXTERNAL)/package/ember_vdr/$(call qstrip,$(EMBER_VDR_PK_INITSCRIPT)) $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_VDR_PK_NAME))/Run/$(call qstrip,$(EMBER_VDR_PK_INITSCRIPT))
endef

$(eval $(generic-package))
