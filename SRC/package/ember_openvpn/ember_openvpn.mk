################################################################################
#
# ember_openvpn
#
################################################################################

EMBER_OPENVPN_VERSION = $(OPENVPN_VERSION)
EMBER_OPENVPN_SITE = $(BR2_EXTERNAL)/package/ember_openvpn/src
EMBER_OPENVPN_SITE_METHOD = local
EMBER_OPENVPN_INSTALL_STAGING = NO
EMBER_OPENVPN_INSTALL_TARGET = YES

EMBER_OPENVPN_DEPENDENCIES = openvpn
EMBER_OPENVPN_PK_NAME = "OpenVPN"
EMBER_OPENVPN_PK_VERSION = $(OPENVPN_VERSION)
EMBER_OPENVPN_PK_DESCRIPTION = "Open-source software application that implements virtual private network (VPN) techniques for creating secure point-to-point or site-to-site connections in routed or bridged configurations and remote access facilities."
EMBER_OPENVPN_PK_DEPENDENCIES = ""
EMBER_OPENVPN_PK_REQUIREDFW = "2.0.0"
EMBER_OPENVPN_PK_DOWNLOAD = "package-OpenVPN.tar.gz"
EMBER_OPENVPN_PK_INITSCRIPT = "POpenVPN"



ifeq ($(BR2_ARCH),"arm")
PACKAGE_ARCH = armv7l
else
PACKAGE_ARCH = $(BR2_ARCH)
endif
OUTPUTDIR = $(BASE_DIR)/../../PACKAGES-$(PACKAGE_ARCH)

define EMBER_OPENVPN_INSTALL_TARGET_CMDS
	mkdir -p $(OUTPUTDIR)
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/Programs/$(call qstrip,$(EMBER_OPENVPN_PK_NAME))
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/Run
	rm -rf $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/Programs/$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/package_info

	echo '[$(call qstrip,$(EMBER_OPENVPN_PK_NAME))]' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/Programs/$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/package_info
	echo 'name = $(EMBER_OPENVPN_PK_NAME)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/Programs/$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/package_info
	echo 'description = $(EMBER_OPENVPN_PK_DESCRIPTION)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/Programs/$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/package_info
	echo 'version = "$(EMBER_OPENVPN_PK_VERSION)"' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/Programs/$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/package_info
	echo 'dependencies = $(EMBER_OPENVPN_PK_DEPENDENCIES)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/Programs/$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/package_info
	echo 'requiredFW = $(EMBER_OPENVPN_PK_REQUIREDFW)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/Programs/$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/package_info
	echo 'download = $(EMBER_OPENVPN_PK_DOWNLOAD)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/Programs/$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/package_info

	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/Programs/bin
	cp -rfv $(BR2_EXTERNAL)/package/ember_openvpn/src/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/Programs/$(call qstrip,$(EMBER_OPENVPN_PK_NAME))
	$(INSTALL) -m 0755 -D $(BR2_EXTERNAL)/package/ember_openvpn/$(call qstrip,$(EMBER_OPENVPN_PK_INITSCRIPT)) $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/Run/$(call qstrip,$(EMBER_OPENVPN_PK_INITSCRIPT))
	$(INSTALL) -m 0755 -D $(TARGET_DIR)/usr/sbin/openvpn $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/Programs/bin/openvpn
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/Configs/OpenVPN/
	touch $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_OPENVPN_PK_NAME))/Configs/OpenVPN/.place_config_files_here
endef

$(eval $(generic-package))
