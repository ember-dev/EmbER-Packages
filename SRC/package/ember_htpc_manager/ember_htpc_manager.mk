################################################################################
#
# ember_htpc_manager
#
################################################################################

EMBER_HTPC_MANAGER_VERSION = be4ecaeae78412f32c4fdf67cb6c82db2e04ecc3
EMBER_HTPC_MANAGER_SITE = git@github.com:Hellowlol/HTPC-Manager.git
EMBER_HTPC_MANAGER_SITE_METHOD = git
EMBER_HTPC_MANAGER_INSTALL_STAGING = NO
EMBER_HTPC_MANAGER_INSTALL_TARGET = YES

EMBER_HTPC_MANAGER_PK_NAME = "HTPC-Manager"
EMBER_HTPC_MANAGER_PK_VERSION = 15.10.24
EMBER_HTPC_MANAGER_PK_DESCRIPTION = "Manage your HTPC from anywhere! A python based web application to manage the software on your HTPC. HTPC Manager combines all your favorite software into one slick web interface. (Default Port: 8085)"
EMBER_HTPC_MANAGER_PK_DEPENDENCIES = ""
EMBER_HTPC_MANAGER_PK_REQUIREDFW = "2.0.0"
EMBER_HTPC_MANAGER_PK_DOWNLOAD = "package-HTPC-Manager.tar.gz"
EMBER_HTPC_MANAGER_PK_INITSCRIPT = "PHTPC-Manager"



ifeq ($(BR2_ARCH),"arm")
PACKAGE_ARCH = armv7l
else
PACKAGE_ARCH = $(BR2_ARCH)
endif
OUTPUTDIR = $(BASE_DIR)/../../PACKAGES-$(PACKAGE_ARCH)

define EMBER_HTPC_MANAGER_INSTALL_TARGET_CMDS
	mkdir -p $(OUTPUTDIR)
	rm -rf $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/Programs/$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/Run

	echo '[$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))]' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/Programs/$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/package_info
	echo 'name = $(EMBER_HTPC_MANAGER_PK_NAME)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/Programs/$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/package_info
	echo 'description = $(EMBER_HTPC_MANAGER_PK_DESCRIPTION)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/Programs/$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/package_info
	echo 'version = "$(EMBER_HTPC_MANAGER_PK_VERSION)"' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/Programs/$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/package_info
	echo 'dependencies = $(EMBER_HTPC_MANAGER_PK_DEPENDENCIES)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/Programs/$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/package_info
	echo 'requiredFW = $(EMBER_HTPC_MANAGER_PK_REQUIREDFW)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/Programs/$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/package_info
	echo 'download = $(EMBER_HTPC_MANAGER_PK_DOWNLOAD)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/Programs/$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/package_info

	cp -rf $(@D)/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/Programs/$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))
	cp -rfv $(BR2_EXTERNAL)/package/ember_htpc_manager/src/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/Programs/$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))
	$(INSTALL) -m 0755 -D $(BR2_EXTERNAL)/package/ember_htpc_manager/$(call qstrip,$(EMBER_HTPC_MANAGER_PK_INITSCRIPT)) $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HTPC_MANAGER_PK_NAME))/Run/$(call qstrip,$(EMBER_HTPC_MANAGER_PK_INITSCRIPT))
endef

$(eval $(generic-package))
