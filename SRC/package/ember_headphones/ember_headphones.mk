################################################################################
#
# ember_headphones
#
################################################################################

EMBER_HEADPHONES_VERSION = v0.5.9
EMBER_HEADPHONES_SITE = git@github.com:rembo10/headphones.git
EMBER_HEADPHONES_SITE_METHOD = git
EMBER_HEADPHONES_INSTALL_STAGING = NO
EMBER_HEADPHONES_INSTALL_TARGET = YES

EMBER_HEADPHONES_PK_NAME = "Headphones"
EMBER_HEADPHONES_PK_VERSION = 0.5.9
EMBER_HEADPHONES_PK_DESCRIPTION = "An automated music downloader for NZB and Torrent, written in Python. It supports SABnzbd, NZBget, Transmission, µTorrent and Blackhole. (Default Port: 8181)"
EMBER_HEADPHONES_PK_DEPENDENCIES = "PythonLibs"
EMBER_HEADPHONES_PK_REQUIREDFW = "2.9.0"
EMBER_HEADPHONES_PK_DOWNLOAD = "package-Headphones.tar.gz"
EMBER_HEADPHONES_PK_INITSCRIPT = "PHeadphones"



ifeq ($(BR2_ARCH),"arm")
PACKAGE_ARCH = armv7l
else
PACKAGE_ARCH = $(BR2_ARCH)
endif
OUTPUTDIR = $(BASE_DIR)/../../PACKAGES-$(PACKAGE_ARCH)

define EMBER_HEADPHONES_INSTALL_TARGET_CMDS
	mkdir -p $(OUTPUTDIR)
	rm -rf $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/Programs/$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/Run

	echo '[$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))]' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/Programs/$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/package_info
	echo 'name = $(EMBER_HEADPHONES_PK_NAME)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/Programs/$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/package_info
	echo 'description = $(EMBER_HEADPHONES_PK_DESCRIPTION)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/Programs/$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/package_info
	echo 'version = "$(EMBER_HEADPHONES_PK_VERSION)"' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/Programs/$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/package_info
	echo 'dependencies = $(EMBER_HEADPHONES_PK_DEPENDENCIES)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/Programs/$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/package_info
	echo 'requiredFW = $(EMBER_HEADPHONES_PK_REQUIREDFW)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/Programs/$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/package_info
	echo 'download = $(EMBER_HEADPHONES_PK_DOWNLOAD)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/Programs/$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/package_info

	cp -rf $(@D)/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/Programs/$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))
	cp -rfv $(BR2_EXTERNAL)/package/ember_headphones/src/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/Programs/$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))
	$(INSTALL) -m 0755 -D $(BR2_EXTERNAL)/package/ember_headphones/$(call qstrip,$(EMBER_HEADPHONES_PK_INITSCRIPT)) $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_HEADPHONES_PK_NAME))/Run/$(call qstrip,$(EMBER_HEADPHONES_PK_INITSCRIPT))
endef

$(eval $(generic-package))
