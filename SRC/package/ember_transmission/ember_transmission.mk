################################################################################
#
# ember_transmission
#
################################################################################

EMBER_TRANSMISSION_VERSION = $(TRANSMISSION_VERSION)
EMBER_TRANSMISSION_SITE = $(BR2_EXTERNAL)/package/ember_transmission/src
EMBER_TRANSMISSION_SITE_METHOD = local
EMBER_TRANSMISSION_INSTALL_STAGING = NO
EMBER_TRANSMISSION_INSTALL_TARGET = YES

EMBER_TRANSMISSION_DEPENDENCIES = transmission
EMBER_TRANSMISSION_PK_NAME = "Transmission"
EMBER_TRANSMISSION_PK_VERSION = 0.$(TRANSMISSION_VERSION)
EMBER_TRANSMISSION_PK_DESCRIPTION = "Transmission is an open source, volunteer-based BitTorrent client which features a simple web interface, watch directories and bad peer blocklists. (Default Port: 9091)"
EMBER_TRANSMISSION_PK_DEPENDENCIES = ""
EMBER_TRANSMISSION_PK_REQUIREDFW = "2.0.0"
EMBER_TRANSMISSION_PK_DOWNLOAD = "package-Transmission.tar.gz"
EMBER_TRANSMISSION_PK_INITSCRIPT = "PTransmission"



ifeq ($(BR2_ARCH),"arm")
PACKAGE_ARCH = armv7l
else
PACKAGE_ARCH = $(BR2_ARCH)
endif
OUTPUTDIR = $(BASE_DIR)/../../PACKAGES-$(PACKAGE_ARCH)

define EMBER_TRANSMISSION_INSTALL_TARGET_CMDS
	mkdir -p $(OUTPUTDIR)
	rm -rf $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Programs/$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Run

	echo '[$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))]' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Programs/$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/package_info
	echo 'name = $(EMBER_TRANSMISSION_PK_NAME)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Programs/$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/package_info
	echo 'description = $(EMBER_TRANSMISSION_PK_DESCRIPTION)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Programs/$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/package_info
	echo 'version = "$(EMBER_TRANSMISSION_PK_VERSION)"' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Programs/$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/package_info
	echo 'dependencies = $(EMBER_TRANSMISSION_PK_DEPENDENCIES)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Programs/$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/package_info
	echo 'requiredFW = $(EMBER_TRANSMISSION_PK_REQUIREDFW)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Programs/$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/package_info
	echo 'download = $(EMBER_TRANSMISSION_PK_DOWNLOAD)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Programs/$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/package_info

	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Programs/bin
	cp -rfv $(BR2_EXTERNAL)/package/ember_transmission/src/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Programs/$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Programs/$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/libs
	cp -rfv $(TARGET_DIR)/usr/lib/libcurl* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Programs/$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/libs
	cp -rfv $(TARGET_DIR)/usr/lib/libevent* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Programs/$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/libs
	cp -rfv $(TARGET_DIR)/usr/share/transmission/web $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Programs/$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))
	$(INSTALL) -m 0755 -D $(BR2_EXTERNAL)/package/ember_transmission/$(call qstrip,$(EMBER_TRANSMISSION_PK_INITSCRIPT)) $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Run/$(call qstrip,$(EMBER_TRANSMISSION_PK_INITSCRIPT))
	$(INSTALL) -m 0755 -D $(TARGET_DIR)/usr/bin/event_rpcgen.py $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Programs/bin/event_rpcgen.py
	cp -rfv $(TARGET_DIR)/usr/bin/transmission* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TRANSMISSION_PK_NAME))/Programs/bin
endef

$(eval $(generic-package))
