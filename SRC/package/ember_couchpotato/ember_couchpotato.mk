################################################################################
#
# ember_couchpotato
#
################################################################################

EMBER_COUCHPOTATO_VERSION = 711310d
EMBER_COUCHPOTATO_SITE = "git@github.com:RuudBurger/CouchPotatoServer.git"
EMBER_COUCHPOTATO_SITE_METHOD = git
EMBER_COUCHPOTATO_INSTALL_STAGING = NO
EMBER_COUCHPOTATO_INSTALL_TARGET = YES

EMBER_COUCHPOTATO_PK_NAME = "CouchPotatoServer"
EMBER_COUCHPOTATO_PK_VERSION = 2017.03.27
EMBER_COUCHPOTATO_PK_DESCRIPTION = "CouchPotato is an automatic NZB and torrent downloader. You can keep a movies I want list and it will search for NZBs/torrents of these movies every X hours. Once a movie is found, it will send it to SABnzbd or download the torrent to a specified directory. (Default Port: 5050)"
EMBER_COUCHPOTATO_PK_DEPENDENCIES = "PythonLibs.Unrar"
EMBER_COUCHPOTATO_PK_REQUIREDFW = "2.0.0"
EMBER_COUCHPOTATO_PK_DOWNLOAD = "package-CouchPotatoServer.tar.gz"
EMBER_COUCHPOTATO_PK_INITSCRIPT = "PCouchPotatoServer"



ifeq ($(BR2_ARCH),"arm")
PACKAGE_ARCH = armv7l
else
PACKAGE_ARCH = $(BR2_ARCH)
endif
OUTPUTDIR = $(BASE_DIR)/../../PACKAGES-$(PACKAGE_ARCH)

define EMBER_COUCHPOTATO_INSTALL_TARGET_CMDS
	mkdir -p $(OUTPUTDIR)
	rm -rf $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/Programs/$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/Run

	echo '[$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))]' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/Programs/$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/package_info
	echo 'name = $(EMBER_COUCHPOTATO_PK_NAME)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/Programs/$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/package_info
	echo 'description = $(EMBER_COUCHPOTATO_PK_DESCRIPTION)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/Programs/$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/package_info
	echo 'version = "$(EMBER_COUCHPOTATO_PK_VERSION)"' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/Programs/$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/package_info
	echo 'dependencies = $(EMBER_COUCHPOTATO_PK_DEPENDENCIES)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/Programs/$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/package_info
	echo 'requiredFW = $(EMBER_COUCHPOTATO_PK_REQUIREDFW)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/Programs/$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/package_info
	echo 'download = $(EMBER_COUCHPOTATO_PK_DOWNLOAD)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/Programs/$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/package_info

	cp -rf $(@D)/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/Programs/$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))
	cp -rfv $(BR2_EXTERNAL)/package/ember_couchpotato/src/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/Programs/$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))
	$(INSTALL) -m 0755 -D $(BR2_EXTERNAL)/package/ember_couchpotato/$(call qstrip,$(EMBER_COUCHPOTATO_PK_INITSCRIPT)) $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_COUCHPOTATO_PK_NAME))/Run/$(call qstrip,$(EMBER_COUCHPOTATO_PK_INITSCRIPT))
endef

$(eval $(generic-package))
