################################################################################
#
# ember_unrar
#
################################################################################

EMBER_UNRAR_VERSION = 0.0.1
EMBER_UNRAR_SITE = $(BR2_EXTERNAL)/package/ember_unrar/src
EMBER_UNRAR_SITE_METHOD = local
EMBER_UNRAR_INSTALL_STAGING = NO
EMBER_UNRAR_INSTALL_TARGET = YES

EMBER_UNRAR_DEPENDENCIES = unrar
EMBER_UNRAR_PK_NAME = "Unrar"
EMBER_UNRAR_PK_VERSION = $(UNRAR_VERSION)
EMBER_UNRAR_PK_DESCRIPTION = "Archive extraction tool for files with a .rar extension."
EMBER_UNRAR_PK_DEPENDENCIES = ""
EMBER_UNRAR_PK_REQUIREDFW = "2.0.0"
EMBER_UNRAR_PK_DOWNLOAD = "package-Unrar.tar.gz"

ifeq ($(BR2_ARCH),"arm")
PACKAGE_ARCH = armv7l
else
PACKAGE_ARCH = $(BR2_ARCH)
endif
OUTPUTDIR = $(BASE_DIR)/../../PACKAGES-$(PACKAGE_ARCH)

define EMBER_UNRAR_INSTALL_TARGET_CMDS
	mkdir -p $(OUTPUTDIR)
	rm -rf $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_UNRAR_PK_NAME))
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_UNRAR_PK_NAME))/Programs/$(call qstrip,$(EMBER_UNRAR_PK_NAME))
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_UNRAR_PK_NAME))/Run

	echo '[$(call qstrip,$(EMBER_UNRAR_PK_NAME))]' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_UNRAR_PK_NAME))/Programs/$(call qstrip,$(EMBER_UNRAR_PK_NAME))/package_info
	echo 'name = $(EMBER_UNRAR_PK_NAME)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_UNRAR_PK_NAME))/Programs/$(call qstrip,$(EMBER_UNRAR_PK_NAME))/package_info
	echo 'description = $(EMBER_UNRAR_PK_DESCRIPTION)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_UNRAR_PK_NAME))/Programs/$(call qstrip,$(EMBER_UNRAR_PK_NAME))/package_info
	echo 'version = "$(EMBER_UNRAR_PK_VERSION)"' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_UNRAR_PK_NAME))/Programs/$(call qstrip,$(EMBER_UNRAR_PK_NAME))/package_info
	echo 'dependencies = $(EMBER_UNRAR_PK_DEPENDENCIES)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_UNRAR_PK_NAME))/Programs/$(call qstrip,$(EMBER_UNRAR_PK_NAME))/package_info
	echo 'requiredFW = $(EMBER_UNRAR_PK_REQUIREDFW)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_UNRAR_PK_NAME))/Programs/$(call qstrip,$(EMBER_UNRAR_PK_NAME))/package_info
	echo 'download = $(EMBER_UNRAR_PK_DOWNLOAD)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_UNRAR_PK_NAME))/Programs/$(call qstrip,$(EMBER_UNRAR_PK_NAME))/package_info

	cp -rfv $(BR2_EXTERNAL)/package/ember_unrar/src/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_UNRAR_PK_NAME))/Programs/$(call qstrip,$(EMBER_UNRAR_PK_NAME))
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_UNRAR_PK_NAME))/Programs/bin
	$(INSTALL) -m 0755 -D $(TARGET_DIR)/usr/bin/unrar $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_UNRAR_PK_NAME))/Programs/bin/unrar
endef

$(eval $(generic-package))
