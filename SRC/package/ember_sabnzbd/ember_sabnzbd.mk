################################################################################
#
# ember_sabnzbd
#
################################################################################

EMBER_SABNZBD_VERSION = 1.1.0
EMBER_SABNZBD_SITE = git@github.com:sabnzbd/sabnzbd.git
EMBER_SABNZBD_SITE_METHOD = git
EMBER_SABNZBD_INSTALL_STAGING = NO
EMBER_SABNZBD_INSTALL_TARGET = YES

EMBER_SABNZBD_PK_NAME = "SABnzbd"
EMBER_SABNZBD_PK_VERSION = $(EMBER_SABNZBD_VERSION)
EMBER_SABNZBD_PK_DESCRIPTION = "An open source binary newsreader written in Python. (Default Port: 8080)"
EMBER_SABNZBD_PK_DEPENDENCIES = "PAR2.Unrar.PythonLibs"
EMBER_SABNZBD_PK_REQUIREDFW = "2.0.0"
EMBER_SABNZBD_PK_DOWNLOAD = "package-SABnzbd.tar.gz"
EMBER_SABNZBD_PK_INITSCRIPT = "PSABnzbd"



ifeq ($(BR2_ARCH),"arm")
PACKAGE_ARCH = armv7l
else
PACKAGE_ARCH = $(BR2_ARCH)
endif
OUTPUTDIR = $(BASE_DIR)/../../PACKAGES-$(PACKAGE_ARCH)

define EMBER_SABNZBD_INSTALL_TARGET_CMDS
	mkdir -p $(OUTPUTDIR)
	rm -rf $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/Programs/$(call qstrip,$(EMBER_SABNZBD_PK_NAME))
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/Run

	echo '[$(call qstrip,$(EMBER_SABNZBD_PK_NAME))]' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/Programs/$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/package_info
	echo 'name = $(EMBER_SABNZBD_PK_NAME)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/Programs/$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/package_info
	echo 'description = $(EMBER_SABNZBD_PK_DESCRIPTION)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/Programs/$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/package_info
	echo 'version = "$(EMBER_SABNZBD_PK_VERSION)"' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/Programs/$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/package_info
	echo 'dependencies = $(EMBER_SABNZBD_PK_DEPENDENCIES)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/Programs/$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/package_info
	echo 'requiredFW = $(EMBER_SABNZBD_PK_REQUIREDFW)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/Programs/$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/package_info
	echo 'download = $(EMBER_SABNZBD_PK_DOWNLOAD)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/Programs/$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/package_info

	cp -rf $(@D)/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/Programs/$(call qstrip,$(EMBER_SABNZBD_PK_NAME))
	cp -rfv $(BR2_EXTERNAL)/package/ember_sabnzbd/src/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/Programs/$(call qstrip,$(EMBER_SABNZBD_PK_NAME))
	$(INSTALL) -m 0755 -D $(BR2_EXTERNAL)/package/ember_sabnzbd/$(call qstrip,$(EMBER_SABNZBD_PK_INITSCRIPT)) $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_SABNZBD_PK_NAME))/Run/$(call qstrip,$(EMBER_SABNZBD_PK_INITSCRIPT))
endef

$(eval $(generic-package))
