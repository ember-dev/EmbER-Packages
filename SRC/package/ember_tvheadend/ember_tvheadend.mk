################################################################################
#
# ember_tvheadend
#
################################################################################

EMBER_TVHEADEND_VERSION = $(TVHEADEND_VERSION)
EMBER_TVHEADEND_SITE = $(BR2_EXTERNAL)/package/ember_tvheadend/src
EMBER_TVHEADEND_SITE_METHOD = local
EMBER_TVHEADEND_INSTALL_STAGING = NO
EMBER_TVHEADEND_INSTALL_TARGET = YES

EMBER_TVHEADEND_DEPENDENCIES = tvheadend
EMBER_TVHEADEND_PK_NAME = "Tvheadend"
EMBER_TVHEADEND_PK_VERSION = 4.0.8
EMBER_TVHEADEND_PK_DESCRIPTION = "Tvheadend is a TV streaming server and recorder for Linux, FreeBSD and Android supporting DVB-S, DVB-S2, DVB-C, DVB-T, ATSC, ISDB-T, IPTV, SAT>IP and HDHomeRun as input sources. (Default Port: 9081)"
EMBER_TVHEADEND_PK_DEPENDENCIES = ""
EMBER_TVHEADEND_PK_REQUIREDFW = "4.0.1"
EMBER_TVHEADEND_PK_DOWNLOAD = "package-Tvheadend.tar.gz"
EMBER_TVHEADEND_PK_INITSCRIPT = "PTvheadend"



ifeq ($(BR2_ARCH),"arm")
PACKAGE_ARCH = armv7l
else
PACKAGE_ARCH = $(BR2_ARCH)
endif
OUTPUTDIR = $(BASE_DIR)/../../PACKAGES-$(PACKAGE_ARCH)

define EMBER_TVHEADEND_INSTALL_TARGET_CMDS
	mkdir -p $(OUTPUTDIR)
	rm -rf $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/Programs/$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/Run

	echo '[$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))]' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/Programs/$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/package_info
	echo 'name = $(EMBER_TVHEADEND_PK_NAME)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/Programs/$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/package_info
	echo 'description = $(EMBER_TVHEADEND_PK_DESCRIPTION)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/Programs/$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/package_info
	echo 'version = "$(EMBER_TVHEADEND_PK_VERSION)"' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/Programs/$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/package_info
	echo 'dependencies = $(EMBER_TVHEADEND_PK_DEPENDENCIES)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/Programs/$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/package_info
	echo 'requiredFW = $(EMBER_TVHEADEND_PK_REQUIREDFW)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/Programs/$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/package_info
	echo 'download = $(EMBER_TVHEADEND_PK_DOWNLOAD)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/Programs/$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/package_info

	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/Programs/bin
	cp -rfv $(BR2_EXTERNAL)/package/ember_tvheadend/src/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/Programs/$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/Programs/$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/libs
	cp -rfv $(TARGET_DIR)/usr/bin/tvheadend* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/Programs/bin
	$(INSTALL) -m 0755 -D $(BR2_EXTERNAL)/package/ember_tvheadend/$(call qstrip,$(EMBER_TVHEADEND_PK_INITSCRIPT)) $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_TVHEADEND_PK_NAME))/Run/$(call qstrip,$(EMBER_TVHEADEND_PK_INITSCRIPT))
endef

$(eval $(generic-package))
